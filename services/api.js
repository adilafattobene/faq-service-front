import axios from "axios";

export const api = axios.create();

api.interceptors.response.use(undefined, (error) => {
  return Promise.reject(error.response);
});
