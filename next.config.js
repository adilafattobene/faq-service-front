module.exports = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/faq",
        permanent: true,
      },
    ];
  },
};