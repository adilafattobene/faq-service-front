import Button from "@material-ui/core/Button";
import CompanyDialog from "./CompanyDialog";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { useSession, signOut } from "next-auth/client";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function Company({ company, onChange, value, onError }) {
  const classes = useStyles();

  const [session, loading, error] = useSession();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <TextField
              id="name"
              defaultValue={company.name}
              value={value}
              label="Nome da loja"
              disabled
              variant="outlined"
              margin="normal"
              fullWidth
              name="name"
            />
          </Paper>
        </Grid>
        {session?.user?.profileId ===
          "fcec55dc-9d24-4c0d-99ad-c99960660f2c" && (
          <Grid item xs="auto">
            <CompanyDialog
              companyId={company.id}
              onSubmit={onChange}
              onError={onError}
            />
          </Grid>
        )}
      </Grid>
    </div>
  );
}
