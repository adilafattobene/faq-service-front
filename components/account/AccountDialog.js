import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useSession } from "next-auth/client";
import { api } from "../../services/api";

export default function AccountDialog({ onSubmit, onError }) {
  const [open, setOpen] = React.useState(false);

  const [session] = useSession();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const userId = session?.accessToken ? session?.user.id : null;
      const config = session?.accessToken
        ? {
            headers: {
              "x-access-token": session?.accessToken,
            },
          }
        : {};

      const response = await api.put(
        `http://localhost:4000/user/` + userId,
        { id: userId, name: event.target.name.value },
        config
      );
      onSubmit(event.target.name.value);
      setOpen(false);
    } catch (err) {
      switch (err?.status) {
        case 401:
          onError("Algo errado aconteceu. Você não tem permissão para isso.");
          setOpen(false);
          break;
        case 403:
          onError(
            "Algo errado aconteceu. Você não tem permissão para buscar essa informação."
          );
          setOpen(false);
          break;
        case 404:
          onError("Algo errado aconteceu. Usuário não encontrado.");
          setOpen(false);
          break;
        default:
          onError("Algo errado aconteceu. Tente novamente mais tarde");
          setOpen(false);
      }
    }
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Alterar dados
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form onSubmit={handleSubmit}>
          <DialogTitle id="form-dialog-title">Alteração do usuário</DialogTitle>
          <DialogContent>
            <DialogContentText>Insira abaixo o novo nome:</DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Novo nome do usuário"
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              CANCELAR
            </Button>
            <Button type="submit" color="primary">
              ALTERAR
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}
