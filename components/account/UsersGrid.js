import Button from "@material-ui/core/Button";
import { DataGrid } from "@material-ui/data-grid";
import Modal from "@material-ui/core/Modal";
import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

// function rand() {
//   return Math.round(Math.random() * 20) - 10;
// }

// function getModalStyle() {
//   const top = 50 + rand();
//   const left = 50 + rand();

//   return {
//     top: `${top}%`,
//     left: `${left}%`,
//     transform: `translate(-${top}%, -${left}%)`,
//   };
// }

// const useStyles = makeStyles((theme) => ({
//   paper: {
//     position: "absolute",
//     width: 400,
//     backgroundColor: theme.palette.background.paper,
//     border: "2px solid #000",
//     boxShadow: theme.shadows[5],
//     padding: theme.spacing(2, 4, 3),
//   },
// }));

// const editUserPopOver = () => {
//   const classes = useStyles();
//   // getModalStyle is not a pure function, we roll the style only on the first render
//   const [modalStyle] = React.useState(getModalStyle);
//   const [open, setOpen] = React.useState(false);

//   const handleOpen = () => {
//     setOpen(true);
//   };

//   const handleClose = () => {
//     setOpen(false);
//   };

//   const body = (
//     <div style={modalStyle} className={classes.paper}>
//       <h2 id="simple-modal-title">Text in a modal</h2>
//       <p id="simple-modal-description">
//         Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
//       </p>
//       <SimpleModal />
//     </div>
//   );

//   return (
//     <div>
//       <button type="button" onClick={handleOpen}>
//         Open Modal
//       </button>
//       <Modal
//         open={open}
//         onClose={handleClose}
//         aria-labelledby="simple-modal-title"
//         aria-describedby="simple-modal-description"
//       >
//         {body}
//       </Modal>
//     </div>
//   );
// };

const columns = [
  { field: "name", headerName: "Nome", width: 300 },
  { field: "userName", headerName: "Usuário de acesso", width: 300 },
  { field: "profile", headerName: "Perfil", width: 130 },
  // { field: "active", headerName: "Ativo?", width: 130 },
  {
    field: "edit",
    headerName: "-",
    width: 130,
    renderCell: () => (
      <Button
        variant="contained"
        color="primary"
        size="small"
        style={{ marginLeft: 16 }}
        // onClick={editUserPopOver}
        disabled
      >
        Editar
      </Button>
    ),
  },
];

export default function UsersGrid({ users }) {
  const [children, setChildren] = React.useState([]);
  
  useEffect(() => {
    setChildren(() => {
      return users.map((user) => {
        return {
          id: user.id,
          name: user.name,
          userName: user.userName,
          profile: user.profile,
          // active: "sim",
        };
      });
    });
  });

  return (
    <div style={{ height: 400, width: "100%" }}>
      <DataGrid rows={children} columns={columns} pageSize={5} />
    </div>
  );
}
