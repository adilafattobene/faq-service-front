import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import AccountDialog from "./AccountDialog";
import LoginInfoDialog from "./LoginInfoDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function Account({ user, onChange, value, onError }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <TextField
              id="name"
              defaultValue={user.name}
              value={value}
              label="Nome"
              disabled
              variant="outlined"
              margin="normal"
              fullWidth
              name="name"
            />
            {/* <TextField
            id="email"
            defaultValue="<email>"
            label="E-mail"
            disabled
            variant="outlined"
            margin="normal"
            fullWidth
            name="email"
          /> */}
            <TextField
              id="profile"
              defaultValue={user.profile}
              label="Perfil"
              disabled
              variant="outlined"
              margin="normal"
              fullWidth
              name="profile"
            />
          </Paper>
        </Grid>
        <Grid item xs="auto" alignItems="center">
          <Grid container justify="center" spacing={2}>
            <Grid item>
              <AccountDialog onSubmit={onChange} onError={onError} />
            </Grid>
            <Grid item>
              <LoginInfoDialog onError={onError} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
