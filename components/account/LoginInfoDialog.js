import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useSession } from "next-auth/client";
import Divider from "@material-ui/core/Divider";
import { api } from "../../services/api";

export default function LoginInfoDialog({ onError }) {
  const [open, setOpen] = React.useState(false);

  const [session] = useSession();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const userId = session?.accessToken ? session?.user.id : null;
      const config = session?.accessToken
        ? {
            headers: {
              "x-access-token": session?.accessToken,
            },
          }
        : {};

      const body = {
        id: userId,
        login: undefined,
      };

      if (event.target.userName.value) {
        let login = body.login;
        login = { ...login, username: event.target.userName.value };

        body.login = login;
      }

      if (event.target.password.value && event.target.oldPassword.value) {
        let login = body.login;
        login = { ...login, password: event.target.password.value };

        body.login = login;
        body.oldPassword = event.target.oldPassword.value;
      }

      const response = await api.put(
        `http://localhost:4000/user/` + userId + "/login",
        body,
        config
      );

      setOpen(false);
    } catch (err) {
      switch (err?.status) {
        case 401:
          onError("Algo errado aconteceu. Você não tem permissão para isso.");
          setOpen(false);
          break;
        case 403:
          onError(
            "Algo errado aconteceu. Você não tem permissão para buscar essa informação."
          );
          setOpen(false);
          break;
        case 404:
          onError("Algo errado aconteceu. Usuário não encontrado.");
          setOpen(false);
          break;
        default:
          onError("Algo errado aconteceu. Tente novamente mais tarde");
          setOpen(false);
      }
    }
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Alterar Login
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form onSubmit={handleSubmit}>
          <DialogTitle id="form-dialog-title">
            Alteração do usuário de acesso
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="userName"
              label="Novo nome do usuário"
              type="text"
              fullWidth
            />

            <br />
            <br />
            <br />
            <Divider />
          </DialogContent>
          <DialogTitle id="form-dialog-title">Alteração da senha</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="oldPassword"
              label="Senha antiga"
              type="text"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="password"
              label="Nova senha"
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              CANCELAR
            </Button>
            <Button type="submit" color="primary">
              ALTERAR
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}
