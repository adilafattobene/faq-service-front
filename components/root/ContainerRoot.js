import { makeStyles, useTheme } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import AttachMoney from "@material-ui/icons/AttachMoney";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import ExitToApp from "@material-ui/icons/ExitToApp";
import Group from "@material-ui/icons/Group";
import Help from "@material-ui/icons/Help";
import Home from "@material-ui/icons/Home";
import IconButton from "@material-ui/core/IconButton";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import Link from "next/link";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import LocationSearchingIcon from "@material-ui/icons/LocationSearching";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import Today from "@material-ui/icons/Today";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import { useSession, signOut } from "next-auth/client";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function ContainerRoot({ children }) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [session] = useSession();

  const isLoggedIn = session?.accessToken;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        {isLoggedIn && (
          <>
            <List>
              <Link href="/not-implemented" color="primary">
                <ListItem button disabled key="Início" title="Início">
                  <ListItemIcon>
                    <Home />
                  </ListItemIcon>
                  <ListItemText primary="Início" />
                </ListItem>
              </Link>

              <Link href="/not-implemented" color="primary">
                <ListItem
                  button
                  disabled
                  key="Agendamentos"
                  title="Agendamentos"
                >
                  <ListItemIcon>
                    <Today />
                  </ListItemIcon>
                  <ListItemText primary="Agendamentos" />
                </ListItem>
              </Link>
              <Link href="/not-implemented" color="primary">
                <ListItem
                  button
                  disabled
                  key="Rastreamento"
                  title="Rastreamento"
                >
                  <ListItemIcon>
                    <LocationSearchingIcon />
                  </ListItemIcon>
                  <ListItemText primary="Rastreamento" />
                </ListItem>
              </Link>
            </List>
            <Divider />
            <List>
              <Link href="/account" color="primary">
                <ListItem button key="Dados Pessoais" title="Dados Pessoais">
                  <ListItemIcon>
                    <Group />
                  </ListItemIcon>
                  <ListItemText primary="Dados Pessoais" />
                </ListItem>
              </Link>

              {session?.user?.profileId ===
                "fcec55dc-9d24-4c0d-99ad-c99960660f2c" && (
                <Link href="/not-implemented" color="primary">
                  <ListItem button disabled key="Financeiro" title="Financeiro">
                    <ListItemIcon>
                      <AttachMoney />
                    </ListItemIcon>
                    <ListItemText primary="Financeiro" />
                  </ListItem>
                </Link>
              )}

              {session?.user?.profileId ===
                "fcec55dc-9d24-4c0d-99ad-c99960660f2c" && (
                <Link href="/not-implemented" color="primary">
                  <ListItem button disabled key="Mensagens" title="Mensagens">
                    <ListItemIcon>
                      <MailIcon />
                    </ListItemIcon>
                    <ListItemText primary="Mensagens" />
                  </ListItem>
                </Link>
              )}
            </List>
            <Divider />
          </>
        )}

        <List>
          <Link href="/faq" color="primary">
            <ListItem button key="FAQs" title="FAQs">
              <ListItemIcon>
                <Help />
              </ListItemIcon>
              <ListItemText primary="FAQs" />
            </ListItem>
          </Link>
        </List>
        <Divider />
        <List>
          {isLoggedIn ? (
            <Link href="/faq" color="primary">
              <ListItem
                button
                key="Sair"
                title="Sair"
                onClick={() =>
                  signOut({
                    callbackUrl: `${window.location.origin}/faq`,
                  })
                }
              >
                <ListItemIcon>
                  <ExitToApp />
                </ListItemIcon>
                <ListItemText primary="Sair" />
              </ListItem>
            </Link>
          ) : (
            <Link href="/login" color="primary">
              <ListItem button key="Entrar" title="Entrar">
                <ListItemIcon>
                  <ExitToApp />
                </ListItemIcon>
                <ListItemText primary="Entrar" />
              </ListItem>
            </Link>
          )}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}
