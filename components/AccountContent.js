import Account from "./account/Account";
import AccountTreeIcon from "@material-ui/icons/AccountTree";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Company from "./account/Company";
import PersonPinIcon from "@material-ui/icons/AccountBox";
import PropTypes from "prop-types";
import React, { useState } from "react";
import StoreIcon from "@material-ui/icons/Store";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import UsersGrid from "./account/UsersGrid";
import { makeStyles } from "@material-ui/core/styles";
import { useSession, signOut } from "next-auth/client";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { api } from "../services/api";
import Alert from "@material-ui/lab/Alert";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function AccountContent({ userSession }) {
  const classes = useStyles();
  const [user, setUser] = React.useState(userSession);
  const [userName, setUserName] = React.useState();
  const [children, setChildren] = React.useState([]);
  const [company, setCompany] = useState(userSession.company);
  const [companyName, setCompanyName] = useState();
  const [value, setValue] = React.useState(0);
  const [session, loading] = useSession();
  const [errorMessage, setErrorMessage] = useState("");

  const handleChange = async (event, newValue) => {
    setValue(newValue);
    setErrorMessage("");

    const userId = session?.accessToken ? session?.user.id : null;

    if (newValue === 2 && userId) {
      const config = session?.accessToken
        ? {
            headers: {
              "x-access-token": session?.accessToken,
            },
          }
        : {};

      try {
        const response = await api.get(
          `http://localhost:4000/user/` + userId + `/users`,
          config
        );
        setChildren(response.data);
      } catch (err) {
        switch (err?.status) {
          case 401:
            setErrorMessage(
              "Algo errado aconteceu. Você não tem permissão para isso."
            );
            break;
          case 403:
            setErrorMessage(
              "Algo errado aconteceu. Você não tem permissão para buscar essa informação."
            );
            break;
          case 404:
            setErrorMessage("Algo errado aconteceu. Usuário não encontrado.");
            break;
          default:
            setErrorMessage(
              "Algo errado aconteceu. Tente novamente mais tarde"
            );
        }
      }
    }
  };

  const handleChangeCompanyName = (nameChanged) => {
    setCompanyName(nameChanged);
  };

  const handleUserChanges = (userNameChanged) => {
    setUserName(userNameChanged);
  };

  const handleError = (errorMessageReceived) => {
    setErrorMessage(errorMessageReceived);
  };

  const router = useRouter();

  return (
    <div className={classes.root}>
      {errorMessage && (
        <>
          <br />
          <Alert severity="error">{errorMessage}</Alert>
        </>
      )}
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label="Dados da loja" icon={<StoreIcon />} {...a11yProps(0)} />
          <Tab
            label="Dados Pessoais"
            icon={<PersonPinIcon />}
            {...a11yProps(1)}
          />
          {session?.user?.profileId ===
            "fcec55dc-9d24-4c0d-99ad-c99960660f2c" && (
            <Tab
              label="Usuários"
              icon={<AccountTreeIcon />}
              {...a11yProps(2)}
            />
          )}
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Company
          company={company}
          onChange={handleChangeCompanyName}
          value={companyName}
          onError={handleError}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Account
          user={user}
          onChange={handleUserChanges}
          value={userName}
          onError={handleError}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <UsersGrid users={children} />
      </TabPanel>
    </div>
  );
}
