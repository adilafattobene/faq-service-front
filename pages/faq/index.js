import ContainerRoot from "../../components/root/ContainerRoot";
import FaqContent from "../../components/FaqContent";
import axios from "axios";
import { getSession } from "next-auth/client";

export default function Faq({ faqs }) {
  return (
    <ContainerRoot>
      <FaqContent faqs={faqs} />
    </ContainerRoot>
  );
}

export async function getServerSideProps(context) {
  const session = await getSession(context);

  // try {
  const config = session?.accessToken
    ? {
        headers: {
          "x-access-token": session?.accessToken,
        },
      }
    : {};

  const response = await axios.get(`http://localhost:4000/faq`, config);

  // } catch (e) {
  //   // Redirecting to the login page with error messsage in the URL
  //   throw new Error();
  // }

  return {
    props: { faqs: response.data }, // will be passed to the page component as props
  };
}
