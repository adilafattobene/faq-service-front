import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import axios from "axios";

const providers = [
  Providers.Credentials({
    name: "Credentials",
    authorize: async (credentials) => {
      try {
        const response = await axios.post(`http://localhost:4000/login`, {
          password: credentials.password,
          userName: credentials.username,
        });

        if (response) {
          const { token, userId, profileId } = response.data;

          return {
            status: "success",
            data: { token, user: { id: userId, profileId } },
          };
        }
      } catch (e) {
        // Redirecting to the login page with error messsage in the URL
        throw new Error(errorMessage + "&username=" + credentials.username);
      }
    },
  }),
];

const callbacks = {
  async jwt(payload, response) {
    if (response) {
      payload.accessToken = response.data.token;
      payload.user = response.data.user;
    }

    return payload;
  },

  async session(session, jwt) {
    session.user = jwt.user;
    session.accessToken = jwt.accessToken;
    return session;
  },
};

const options = {
  providers,
  callbacks,
  pages: {
    error: "/login", // Changing the error redirect page to our custom login page
  },
  session: {
    maxAge: 60 * 60,
  },
};

export default (req, res) => NextAuth(req, res, options);
