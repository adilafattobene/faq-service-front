import ContainerRoot from "../../components/root/ContainerRoot";
import SignUpContent from "../../components/SignUpContent";

export default function SignUp() {
  return (
    <ContainerRoot>
      <SignUpContent />
    </ContainerRoot>
  );
}