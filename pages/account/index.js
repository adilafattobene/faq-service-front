import AccountContent from "../../components/AccountContent";
import ContainerRoot from "../../components/root/ContainerRoot";
import axios from "axios";
import { getSession } from "next-auth/client";

function Account({ user, error }) {
  return (
    <ContainerRoot>
      {error ? <p>ERROR - Recarregue a página.</p> : <AccountContent userSession={user} />}
    </ContainerRoot>
  );
}

Account.getInitialProps = async (ctx) => {
  const session = await getSession(ctx);

  if (!session) {
    ctx.res.writeHead(302, { Location: "/login" });
    ctx.res.end();
    return {};
  }

  try {
    const userId = session?.accessToken ? session?.user.id : null;
    const config = session?.accessToken
      ? {
          headers: {
            "x-access-token": session?.accessToken,
          },
        }
      : {};

    const response = await axios.get(
      `http://localhost:4000/user/` + userId,
      config
    );

    return { user: response.data }; // will be passed to the page component as props
  } catch (e) {
    // Redirecting to the login page with error messsage in the URL
    return { error: { message: "error" } }; // will be passed to the page component as props
  }
  // throw new Error();
};

Account.auth = true;

export default Account;
