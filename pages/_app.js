import "../styles/globals.css";
import { Provider, useSession } from "next-auth/client";
import { useEffect } from "react";

function MyApp({ Component, pageProps }) {
  console.log({ pageProps });
  return (
    <Provider session={pageProps.session}>
      {Component.auth ? (
        <Auth>
          <Component {...pageProps} />
        </Auth>
      ) : (
        <Component {...pageProps} />
      )}
    </Provider>
  );
}

function Auth({ children }) {
  const [session, loading] = useSession();
  const isLoggedIn = !!session?.accessToken;
  useEffect(() => {
    if (loading) return; // Do nothing while loading
    if (!isLoggedIn) signIn(); // If not authenticated, force log in
  }, [isLoggedIn, loading]);
  if (isLoggedIn) {
    return children;
  }
  // Session is being fetched, or no user.
  // If no user, useEffect() will redirect.
  return <div>Loading...</div>;
}

export default MyApp;
