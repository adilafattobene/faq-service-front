import ContainerRoot from "../../components/root/ContainerRoot";
import LoginContent from "../../components/LoginContent";

export default function Login() {
  return (
    <ContainerRoot>
      <LoginContent />
    </ContainerRoot>
  );
}